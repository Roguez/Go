Aquí lo tienes: **https://golang.org/ref/spec**

Realmente sabiendo lo que quieres de C y Python, y todos los ecosistemas que hay alrededor,
en una semana estás programando con G. No tiene ciencia, una semana es suficiente.
Porque **la sintaxis es tan similar a C**, no hay problema.

-----------------------------------------------------------------
//hello.go

package main

import "fmt"

func main() {
	fmt.Printf("hello, world\n")
}


-----------------------------------------------------------------
$ go build hello.go
$ ./hello

-----------------------------------------------------------------

godoc fmt Println               --- info about Println

-----------------------------------------------------------------
NUMERIC DATA TYPES

Integers:

uint8, uint16, uint32, uint64, int8, int16, int32, and int64.

There are also three machine dependent integer types: uint,
int, and uintptr. They are machine dependent because their size
depends on the type of architecture you are using.

Float:

float32 and float64

Complex:

complex64 and complex128.

Others:

NaN
(−∞, +∞)
-------------------------------------------------------------------
Strings:

"..."

var x string = "Hello, World\n"
fmt.Println(x)

-------------------------------------------------------------------
Variables in Go are created by first using the var keyword, then specifying
the variable name (x) and the type (string)
------------------------------------------------------------------------

Ok **continuar por la página 19** (papel) del Introducing to go.
Cuando se pone a contar que las variables también se pueden declarar así:

x := "Hello, World"
x := 5

que el compilador se dará cuenta de que tipo es(si pero el compilador no
sabe cual es el valor máximo que tiene que tener, así que a mi no me gusta
yo vengo de C y voy a mantener en ese sentido la disciplina de C)

------------------------------------------------------------------------------






-----------------------------------------------------------------









