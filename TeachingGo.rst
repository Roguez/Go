
1- Empezar trabajando aquí los primeros días, ni ide ni compilación ni hostias.
   Todo el código de Get Programming with Go se puede programar desde aquí.
   Imagínate que fácil para la gente!!
   Entonces nos ponemos a jugar ahí que cogan práctica jugando, por ejemplo con
   variables numéricas, haciendolas float, integers etc y viendo lo que pasa
   los mensajes del compilador etc etc, que se acostumbren a jugar con ese 
   bicho en cada clase una sección de probar tonterías y ver lo que pasa para 
   que les resulte cómodo ponerse  a probar cosas ahí y pierdan el miedo. Eso 
   es lo mas importante al principio, perder el miedo abrir la web y ponerse 
   a probar y a cambiar cosas y a ver lo que pasa.
   
        https://play.golang.org/

2- En Go PlayGround time stands still and results are cached, lo que quiere 
   decir que rand te devolverá siempre los mismos números aleatorios.

3- Te pones a trabajar con las variables y con los if-else, bucles, etc 
   demostrando los scopes y jugando con eso, que se acostumbren a practicar
   investigar por si mismos y razonar esas cosas elementales, y borrar y 
   escribir y ver si da el número que ellos quieren o no.


