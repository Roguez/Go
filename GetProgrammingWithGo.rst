- The Go compiler is written in Go.
- Mírate como en todo lenguaje las build-in functions.
- Mírate las constantes del paquete math, como math.MinInt16 y math.MaxInt16
  que te dicen el valor máximo de este tipo y así puedes chequear que estás 
  dentro de rango antes de hacer una conversión de tipos para no tener 
  comportamientos inesperados.
- Go STL, parece que su standard library es fabulosa.

1- Puedes ejecutar código diréctamente aquí, y compartir en enlace:
   https://play.golang.org

2- package
   import 
   func 

3- Print
   Println - line new (no necesita \n como las otras dos)
   Printf  - Permite mas cosas, parecida a printf en C.
             El primer argumento debe ser texto.
             Permite formato, alinear texto etc.
             Usar esta, también trabajo con C.
             
             Con "" te ejecuta las escape sequences like \n
             Con `` te imprime las escape sequences.

             Strings with "" are called conventional strings literals.
             Strings with `` are called raw strings literals.

             raw string literals can span multiple lines of source code:

             fmt.Println(`
                 peace be upon you
                          upon you be peace`)




4- 
   
   var distance = 56000000
   var speed = 100800

   o también:

   var distance, speed = 56000000, 100800

   o también:

   var (
   distance = 56000000
   speed = 100800
   ) 
   
4- var count int = 10

   o también:

   var count = 10
   
   o también:

   count := 10

5- Increment operator:

   age = age + 1
   age += 1
   age++  (++age is not valid, in C both are valid)

6- import ("math/rand")
   means que queremos importar el módulo "rand" pero debemos indicar su 
   ruta desde "math", como vemos es una carpeta dentro del directorio 
   math pero si hicieras solo "math" no se importaría "rand", es decir
   no podrías referir rand.something

7- packagename.funcionname    example:  rand.Intn()
   entonces primero hay que hacer un import packagename.
   import ("math") or import ("math/rand")
   
8- Tienen un paquete "string" guay, estúdiatelo, sección especial.

9- true and false boolean:

  var walkOutside = true
  var takeTheBluePill = false

10- Go provides the switch statement:
 
    switch variablename {
    case   x   :
    case   y   :
        fallthrough
    case   z   :
    default    :
    }

11- for loops:

   - Normal loop:
     
     var count = 10
     for count > 0 {
        fmt.Println(count)
        time.Sleep(time.Second)
        count--
     }

   - Infinite loop, then break:
    
    var degrees = 0
    for {
        fmt.Println(degrees)
        degrees++
        if degrees >= 360 {
                degrees = 0
                if rand.Intn(2) == 0 {
                        break
                }       
        }
     }

   - El de toda la vida:

     Statement 1 is executed **(one time) before** the execution of the code block.
     Statement 2 defines the condition for executing the code block. 
     Statement 3 is executed **(every time) after** the code block has been executed.

     var count = 20
     for count = 10; count > 0; count-- {
         fmt.Println(count)
     }
     fmt.Println(count)

     Entonces queda claro que el primer valor que imprime este loop es *10*
     y fuera del loop *0*.

   - Pero sin embargo en este otro, como declaro la variable count, como parte
     del for block (en su cabecero) el primer valor que me imprime es *10* y
     fuera del loop *20*.

     var count = 20
     for count := 10; count > 0; count-- {
     fmt.Println(count)
     }
     fmt.Println(count)

     Lo único que he hecho ha sido poner dos puntitos ... y entonces he 
     pasado a declarar la variable dentro del bloque loop, porque lo que
     desaparecerá al terminar la ejecución del bloque. 

     En un cabecero de bucle for, if-else statement etc, si quieres 
     declarar una variable lo tienes que hacer con el formato :=
     porque no es válido usar en esa parte del programa la keyword var.

12- Scoping rules:

    In Go, scopes tend to begin and end along the lines of curly braces {}.
    And its respectives headers, lo que quiere decir que la declaración de
    una variable en una cabecera the bloque loop for or if-statement, solo 
    tiene vida dentro de ese bloque. 

    Los bloques anidados puede ver y utilizar el valor de las variables
    de sus bloque host y así en cadena, pero no va en la dirección contraria
    porque al terminar un bloque se muere una variable.

    Puedo en un bloque declarar una variable con el mismo nombre que otra
    en su bloque host pero no son en realidad la misma variable entonces, 
    porque lo que ni la sobreescribo ni puedo leerla ni nada (la del bloque
    host)

    Una variable declarada fuera de cualquier bloque está disponible en 
    todo el paquete (variable global) Importante: short declaration is not 
    available for global variables (declared to the whole packed)

    En un bucle for ya sabes como sería. Con if-else statement o 
    switch-case statements sería así de guay:

    Normalmente es (simplificando):

   if num == 0 {
   }
     else if scenario {
     }
     else {
     }

   Pues bien, lo que hacemos es:

   if num := rand.Intn(10); num == 0 {
   }   ....
       ....
       ....

  Y lo mismo con un switch-case, normalmente sería:

  switch num {
      cases ..
       ...
      ..
  }

  Y yo lo que hacemos es que le metermos esto:

  switch num := rand.Intn(10); num { 
       ...
        ...
         ...
  }

   The **case** and **default** keywords also introduce a new scope even 
   though no curly braces are involved.


--------------------------------------------------------------------------------------

Unit 2 - types
--------------

Go has 15 numeric types.

- Floats:

days := 365.2425

var days = 365.2425

var days float64 = 365.2425

var answer float64 = 42

Functions in the math package operate on float64 types, so prefer float64 unless you have a good reason to do otherwise.

The zero value

In Go, each type has a default value, called the zero value. The default applies when you declare a variable but don’t initialize it with a value.

When using Print or Println with floating-point types, the default behavior is to display as many digits as possible.

You can use Printf with the %f formatting verb to specify the number of digits.

third := 1.0 / 3
fmt.Println(third)               // Prints 0.3333333333333333
fmt.Printf("%v\n", third)        // Prints 0.3333333333333333
fmt.Printf("%f\n", third)        // Prints 0.333333
fmt.Printf("%.3f\n", third)      // Prints 0.333
fmt.Printf("%4.2f\n", third)     // Prints 0.33
fmt.Printf("%05.2f\n", third)    // Prints 00.33   (Zero padding)

width.precision

width is the total amount of characters including the dot.
precision is the numbers or characters to the right of the dot.

Floating-point inaccuracies:

piggyBank := 0.1
piggyBank += 0.2
fmt.Println(piggyBank)           // Prints 0.30000000000000004

As you can see, floating-point isn’t the best choice for representing money.
One alternative is to store the number of cents with an integer type

To minimize rounding errors, we recommend that you perform multiplication before
division.

Comparing floating points numbers:

piggyBank := 0.1
piggyBank += 0.2
fmt.Println(piggyBank == 0.3)                    // Prints false
fmt.Println(math.Abs(piggyBank-0.3) < 0.0001)    // Prints True

No podemos comparar dos flotantes directamente, sino calcular el valor absoluto
de la diferencia entre ambos y luego determinar si esa diferencia es lo suficientemente
pequeña (epsilon), para lo que debemos establecer el valor de ese epsilon.

The upper bound for a floating-point error for a single operation is known as the
machine epsilon, which is 2-52 for float64 and 2-23 for float32. Unfortunately, floating-point
errors accumulate rather quickly. Add 11 dimes ($0.10 each) to a fresh piggyBank, and the
rounding errors exceed 2-52 when compared to $1.10. That means you’re better off picking
a tolerance specific to your application—in this case, 0.0001. 

Otro ejemplo mas:

piggyBank := 0.0
for i := 0; i < 11; i++ {
piggyBank += 0.1
}
fmt.Println(piggyBank)    // Prints: 1.0999999999999999

- 8 kinds of integers:

Type                           Range                       Storage
int8                        –128 to 127                8-bit (one byte)
uint8                          0 to 255
int16                    –32,768 to 32,767            16-bit (two bytes)
uint16                         0 to 65535
int32             –2,147,483,648 to 2,147,483,647     32-bit (four bytes)
uint32                         0 to 4,294,967,295
int64 –9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 64-bit (eight bytes)
uint64                         0 to 18,446,744,073,709,551,615        (18e18)

- The Printf function provides the %T format verb to display a variable’s type:

year := 2018
fmt.Printf("Type %T for %v\n", year, year)    // Prints Type int for 2018

But how many bytes have the int type?

There are two integer types not listed in the table. The int and uint types are optimal
for the target device. The Go Playground, Raspberry Pi 2, and older mobile phones provide
a 32-bit environment where both int and uint are 32-bit values. Any recent computer
will provide a 64-bit environment where int and uint will be 64-bit values.

So, int and uint could be either 32-bit or 64-bit, depending on the underlying hardware.

Hexadecimals
------------

To distinguish between decimal and hexadecimal, Go requires a 0x prefix for hexadecimal.
These two lines of code are equivalent:

var red, green, blue uint8 = 0, 141, 213
var red, green, blue uint8 = 0x00, 0x8d, 0xd5

To print on hexadecimal use %x or %X.

Padding to print on a particular width:

	year := 2018
fmt.Printf(" %x\n", year)      // Prints: 7e2
fmt.Printf(" %05x\n", year)    // Prints: 007e2

When that range is exceeded, integer types in Go wrap around, pero ni te creas ni confíes 
mucho en esto, ya que Go programs and libraries are often written assuming "reasonable inputs"
and no overflow y cuando ocurre un overflow entonces suele haber un panic como cuando se 
divide por cero en vez de un sencillo wrap around.

The %b format verb will show you the bits for an integer value. guay! Y también acepta padding:

%08b         // porque aunque sea un uint8, el solo te va a mostrar los bits significativos
             // por ejemplo si es 3, solo vas a ver 11, por eso es útil el padding aquí.
%064b

Integer types need to be chosen carefully to avoid wrapping around.

Go big package:

    The big package provides three types:
    
        big.Int is for big integers, when 18 quintillion isn’t enough.
        big.Float is for arbitrary-precision floating-point numbers.
        big.Rat is for fractions like ⅓.

    We need to import:

        "math/big"

    Opting to use big.Int requires that you use it for everything in your equation (even
    the first member), even the constants.

    The NewInt function takes an int64 and returns a big.Int

    secondsPerDay := big.NewInt(86400)

    If it won’t fit in an int64, so instead you can create a big.Int from a string:

    distance := new(big.Int)
    distance.SetString("24000000000000000000", 10)   // The second argument is the base.

Constants of unusual size:

    const distance = 24000000000000000000

    Constants are declared with the const keyword, but every literal value in your program is a constant too.

    That means unusually sized numbers can be used. 

    But if we declare the constant with a type then we will have overflow if we try to represent a bigger number.

    const distance uint64 = 24000000000000000000         // we would have here overflow 24e18

    Under the hood, untyped numeric constants are backed by the big package.

    So for instance:

    const distance = 24000000000000000000
    const lightSpeed = 299792
    const secondsPerDay = 86400
    const days = distance / lightSpeed / secondsPerDay
    fmt.Println("Andromeda Galaxy is", days, "light days away.")

    or:

    fmt.Printf("Andromeda Galaxy is %v light days away.", days)   // se imprime como un int mas.

    Constant values can be assigned to variables so long as they fit.

Strings

    Declaration

    peace := "peace"
    var peace = "peace"
    var peace string = "peace"

If you declare a variable without providing a value, it will be initialized with the zero
value for its type. The zero value for the string type is an empty string (""):

Mira que chupi guay, fíjate en el uso de %T, como habilita un espacio en blanco y todo:

fmt.Printf("%v is a %[1]T\n", "literal string")       //print: literal string is a string
fmt.Printf("%v is a %[1]T\n", `raw string literal`)   //print: raw string literal is a string.

Characters, code points, runes, and bytes:

   type byte = uint8
   type rune = int32

Both byte and rune behave like the integer types.

To display the characters rather than their numeric values, the %c format verb can be
used with Printf:

     a := 234
     var a2 rune = 234

     fmt.Printf("\t %v, %v\n", a, a2)      // prints: 234, 234
     fmt.Printf("\t %c, %c", a, a2)        // prints: ê, ê

¿Entonces para que diablos usar rune or byte?.......

Rather than memorize Unicode code points, Go provides a character literal.
Just enclose a character in single quotes 'é'

Aacute := 'é'
fmt.Printf("%c %[1]v\n", acute)'     // prints ê 234 (las dos cosas)

Pulling the strings in Go:

message := "shalom"
c := message[5]
fmt.Printf("%c\n", c)         // prints m

Strings in Go are immutable, as they are in Python, Java, and JavaScript. Unlike strings in
Ruby and character arrays in C, you can’t modify a string in Go:

message[5] = 'd'       !!!!! wrong, Cannot assign to message[5]

Cesar cipher: One effective method of sending secret messages in the
second century was to shift every letter.

c := 'a'
c = c + 3
fmt.Printf("%c", c)    // prints d

Strings in Go are encoded with UTF-8, one of several encodings for Unicode code
points. UTF-8 is an efficient variable length encoding where a single code point may use
8 bits, 16 bits, or 32 bits. By using a variable length encoding, UTF-8 makes the transition
from ASCII straightforward, because ASCII (8 bits) characters are identical to their UTF-8
encoded counterparts.

The first step to supporting other languages is to decode characters to the rune type
before manipulating them.The DecodeRuneInString function returns the first character and the number of bytes the character consumed. We need to import "unicode/utf8"


....
....

No termino este apartado, sino que sigo avanzando.

Chapter 11, Converting between types:

If you have variables of different types, you must convert the values to the same type before they can be used together.

Type conversion is straightforward:

age := 41
marsAge := float64(age)

But be carefully with conversions, we could go out of range for example from float to
integer:

var bh float64 = 32767
var h = int16(bh)
fmt.Println(h)

If the value of bh is 32,768, which is too big for an int16, the result is what we’ve 
come to expect of integers in Go: it wraps around, becoming the lowest possible number
for an int16, –32768.

Si fuera el 32,769 sería: -32767  y así.
Si fuera el 32,770 sería: -32766  y así.

Y así, entonces en vez de tener una excepción tenemos un wrapping around.

Strings convertions:

To convert a rune or byte to a string, you can use the same type conversion syntax as
numeric conversions.

Usa la built-in function: string()

To convert digits to a string, usa  Itoa from strconv package: strconv.Itoa()

Itoa means integer to ASCII

Another way to convert a number to a string is to use Sprintf, a cousin of Printf that
returns a string rather than displaying it:

countdown := 9
str := fmt.Sprintf("Launch in T minus %v seconds.", countdown)
fmt.Println(str)         // Prints Launch in T minus 9 seconds.

Unit 3 Building blocks
----------------------

Functions:

   func Intn(n int) (int, int, float) {code} // Go functions can return multiple parameters.

Methods: 

    At first it may look like methods are just a different syntax for doing what functions
already do, and you would be right. Later lessons, those in unit 5 in particular, 
demonstrate how methods can be combined with other language features to bring new capabilities.

Types and methods provide another useful way to organize code and represent the world 
around you.

Types can't be mixed:

type celsius float64
type fahrenheit float64
var c celsius = 20
var f fahrenheit = 20
if c == f {            // Invalid operation:mismatched types celsius and fahrenheit
}
c += f

¿cómo se hace conversión de tipos? pues automáticamente poniendo siemplemente el nombre
del type:

grados_celsius := celsius(f)  // Así de fácil

For decades classical object-oriented languages have taught that methods belong with
classes. Go is different. There are no classes or even objects, really, yet Go has
methods.

Methods in Go are actually more flexible than in languages of the past.

Declaring a method to convert kelvin to celsius is as easy as declaring a function.
They both begin with the func keyword, and the function body is identical to the method
body:

type kelvin float64
type celsius float64

func kelvinToCelsius(k kelvin) celsius {  //declaring a function
return celsius(k - 273.15)
}

func (k kelvin) celsius() celsius {       //declaring a method on the kelvin type
return celsius(k - 273.15)                // convierte kelvin a celsius
}

func: palabra clave
(k kelvin):recibidor
k:name
kelvin:type
celsius(): nombre del método
celsius: resultado data type

We can associate methods with any type declared in the same package, but not with
predeclared types (int, float64, and so forth).

The syntax to use a method is different than calling a function:

var k kelvin = 294.0
var c celsius
c = kelvinToCelsius(k)   // Calling a function
c = k.celsius()          // Calling a method

So, a variable of the correct type is followed by a dot and the method name.

Methods can accept multiple parameters and return multiple results, just like 
functions, but they must always have exactly one receiver. Within the method 
body, the receiver behaves just like any other parameter.

A package can only have a single function with a given name, and it can’t be the
same name as a type, so a celsius function that returns a celsius type isn’t
possible. But each temperature type (celsius, kelvin, farenheit) can provide a 
celsius method. So we would write a celsius method for kelvin type and another
one for farenheit type. Really it sounds easy, and people don't need complications
with classes.

FIRST-CLASS FUNCTIONS

In Go you can assign functions to variables, pass functions to functions, and even write
functions that return functions. Functions are first-class—they work in all the places that
integers, strings, and other types work.

Una función se pasa así:

func measureTemperature(samples int, s func() kelvin)

Pero si la declaras como un tipo:

type sensor func() kelvin

Entonces la puedes pasar así:

func measureTemperature(samples int, s sensor)

But it may not been always like an improvement, as you now need to know what
sensor is when looking at this line of code.

Closures and anonymous functions:

An anonymous function, also called a function literal in Go, is a function without a name.

You can assign an anonymous function to a variable and then use that variable like any
other function.

The variable you declare can be in the scope of the package or within a function, as
shown in the next listing.

var f = func() {fmt.Println("Dress up for the masquerade.")}  

f()      // print Dress up for the masquerade

También es posible declarar e invocar a una función anónima en un solo paso:

func() { fmt.Println("Functions anonymous")}()   //los paréntesis del final hacen la 
                                                 //invocación.

One good circumstance to use anonimous function is when returning a function from another
function. Although it’s possible for a function to return an existing named function, 
declaring and returning a new anonymous function is much more useful.

return func() kelvin {     // Declare and return an anonymous function.
return s() + offset
}

Unit 4 - Collections

Declare an array:

var name [size]type

Declare an array and initialize it with a composite literal.

dwarfs := [5]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}

With larger arrays, breaking the composite literal across multiple lines can be more
readable.

planets := [...]string{             // Si, lo de los 3 puntos es tal cual, funciona.
"Mercury",
"Venus",
"Earth",
"Mars",
"Jupiter",
"Saturn",
"Uranus",
"Neptune",     // the trailing come is required
}

Assigning an array to a new variable or passing it to a function makes a complete copy
of its contents.

When we are passing an array to a function we also have to declare the size of the array in the
function arguments definitions, as the length of an array is part of its type.

For these reasons, arrays aren’t used as function parameters nearly as often as slices, covered
in the next lesson.

multidimentional arrays:

var name [size][size]type

We can iterate over arrays with for loops or range keyword, which is more convenient to avoiding
errors with <=  or < regarding the last index, so:

So, sea:

var planets [8]string

.... inicializado:

for i := range planets {
     .... planets[i] ....
}

es equivalente a:

for i := 0; i < len(planets); i++ {
     .... planets[i] ....
}

SLICES: WINDOWS/VIEWS INTO ARRAYS

array[0:4]   // from 0 to 3 (so it takes 4 elements)(so, it include the first but not the last)
array[4:6]   // from 4 to 5 (so it takes 2 elements)(so, it include the first but not the last)
array[:]     // te imprime todo el array.
array[:4]    // te imprime desde el principio hasta el 3
array[3:]    // te imprime desde 3 hasta el final

Mira que mierda:

question := "¿Cómo estás?"
fmt.Println(question[:6])    // prints ¿Cóm 
                             // the indices indicate the number of bytes, not runes.
So Go fuckint strings are even worst than in other languages! a fucking shit!
In Go language, strings are different from other languages like Java, C++, Python, 
etc. It is a sequence of variable-width characters where each and every character is
represented by one or more bytes using UTF-8 Encoding.   

Entonces, hasta aquí llego con los strings! que le den! mirarlo mas adelante!
Hay mucho que mirar y practicar, menuda mierda!!!!

Declaring a slice directly:
---------------------------

Slicing an array is one way to create a slice, but you can also declare a slice directly.
A slice of strings has the type []string, with no value between the brackets. This differs
from an array declaration, which always specifies a fixed length or ellipsis between the
brackets.

dwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}

There is still an underlying array. Behind the scenes, Go declares a five-element array
and then makes a slice that views all of its elements.

So,

dwarfArray := [...]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}

dwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}

fmt.Printf("\t dwarfArray: %T, dwarfs: %T", dwarfArray, dwarfs)

   // prints:  dwarfArray: [5]string, dwarfs: []string

      so

      [n]string = array string
      []string = slice string

This always create a slice:

dwarfArray := [...]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}

dwarfSlice := dwarfArray[:]         // So dwarfSlice es un slice pero con todos los miembros del array además.

So it is important to understand that arrays and slices are diferent types in Go.

The power of slices
-------------------

Slices are more versatile than arrays:

1) slices have a length, but unlike arrays, the length isn’t part of the type. So, we can pass an slice of any
   size to a function. Arrays are rarely used directly.

2) Tu tienes un arrray, te haces un slice de ese array y lo pasas como argumento en una función, cualquier 
   cambio que hagas a los elementos de ese slice se van a ver reflejados en el array del que son imagen aunque 
   esté fuera del scope de la fución.

   Por consiguiente cualquier otro slice del array verá los cambios también.

Slices with methods
-------------------

In Go you can define a type with an underlying slice or array. Once you have a type, you can attach methods to
it. Go’s ability to declare methods on types proves more versatile than the classes of other languages.

A bigger slice
--------------

Arrays have a fixed number of elements, and slices are just views into those fixed-length arrays.

dwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
dwarfs = append(dwarfs, "Salacia", "Quaoar", "Sedna")

The dwarfs slice began as a view into a five-element array, yet the preceding code appends four more elements.
How is that possible?

The number of elements that are visible through a slice determines its length. If a slice
has an underlying array that is larger, the slice may still have capacity to grow.

len(slice_name)
cap(slice_name)

When the array backing dwarfs1 doesn’t have enough room (capacity) to append Orcus, so append copies the contents
of dwarfs1 to a freshly allocated array with twice the capacity.

dwarfs1 := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}    // Length 5, capacity 5
dwarfs2 := append(dwarfs1, "Orcus")                                    // Length 6, capacity 10
dwarfs3 := append(dwarfs2, "Salacia", "Quaoar", "Sedna")               // Length 9, capacity 10

To demonstrate that dwarfs2 and dwarfs3 refer to a different array than dwarfs1, simply modify an element and print
out the three slices.


Three-index slicing

Sea planets un array de 8 elementos por ejemplo, podemos hacer un slice y a la vez fijar su capacidad a una diferente
a la del array al que apunta. Esto tiene implicaciones futuras a la hora de utilizar apend por ejemplo, porque en vez
de tocar el array inicial te estaría trabajando con el array nuevo que se tuvo que crear para acomodar ese quinto 
elemento ya que la capacidad del slice es 4 (la misma que su tamaño, es decir, ya está a full).

terrestrial := planets[0:4:4]                  // Length 4, capacity 4

So, Unless you want to overwrite the original array, you should default to three-index slicing whenever you take a 
slice.

make built-in function:

dwarfs := make([]string, 0, 10)
dwarfs = append(dwarfs, "Ceres", "Pluto", "Haumea", "Makemake", "Eris")

The capacity argument is optional. To start with a length and capacity of 10, you can use
make([]string, 10). Each of the 10 elements will contain the zero value for their type, an
empty string in this case.

and here:

newWorlds := make([]string, len(worlds))       // we make a new slice instead of modifying worlds directly

Declaring variadic functions:

Variadic functions accept a variable number of arguments which are placed in a slice.
To declare a variadic function, use the ellipsis … with the last parameter.

func func_name(arg_name arg_type, name ...data_type) data_type {}

y para llamarla:

twoWorlds := terraform("New", "Venus", "Mars")   // los valores sueltos

planets := []string{"Venus", "Mars", "Jupiter"}
newPlanets := terraform("New", planets...)       // o pasándole un slice
   
map (dictionaries in Python)
----------------------------

Whereas arrays and slices are indexed by sequential integers, map keys can be nearly any type.

To declare a map with keys of type string and values of type int, the syntax is:

map[string]int

Use square brackets [] to look up values by key, to assign over existing values, or to add
values to the map.




















































