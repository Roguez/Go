Concurrency is difficult and warrants a carefull study.

Concurrent code is notoriously difficult to get right. It usually takes a few iterations to
get it working as expected, and even then it’s not uncommon for bugs to exist in code
for years before some change in timing (heavier disk utilization, more users logged
into the system, etc.) causes a previously undiscovered bug to rear its head.

Data race
---------
Where one concurrent operation attempts to read a variable while at some undetermined time 
another concurrent operation is attempting to write to the same variable.

Data races are introduced because the developers are thinking about the problem sequentially.
They assume that because a line of code falls before another that it will run first.

When writing concurrent code, you have to meticulously iterate through the possible
scenarios. Unless you’re utilizing some of the techniques we’ll cover later in the book,
you have no guarantees that your code will run in the order it’s listed in the sourcecode.

For instance:

1 var data int
2 go func() {
3       data++
4 }()
5 if data == 0 {
6       fmt.Printf("the value is %v.\n", data)
7 }

There are three possible outcomes to running
this code:

• Nothing is printed. In this case, line 3 was executed before line 5.
• “the value is 0” is printed. In this case, lines 5 and 6 were executed before line 3.
• “the value is 1” is printed. In this case, line 5 was executed before line 3, but line 3
was executed before line 6.

I sometimes find it helpful to imagine a large period of time passing between
operations. Imagine an hour passes between the time when the goroutine is invoked,
and when it is run. How would the rest of the program behave?

In short we have Data Race when the order of operations is nondeterministic.
solving data races, doesn't mean that we have actually solved the races conditions!
And what we need is to avoid any kind of race condition.

Atomicity
---------

When something is considered atomic, or to have the property of atomicity, this
means that within the context that it is operating, it is indivisible, or uninterruptible.

Operations that are atomic within the context of your
process may not be atomic in the context of the operating system; operations that are
atomic within the context of the operating system may not be atomic within the context
of your machine; and operations that are atomic within the context of your
machine may not be atomic within the context of your application.

When thinking about atomicity, very often the first thing you need to do is to define
the context, or scope, the operation will be considered to be atomic in. Everything
follows from this.

**Indivisible** and **uninterruptible**. These terms mean that
within the context you’ve defined, something that is atomic will happen in its entirety
without anything happening in that context simultaneously.

i++ may look atomic, but a brief analysis reveals several operations:

• Retrieve the value of i.
• Increment the value of i.
• Store the value of i.

If your context is a program with no concurrent processes, then this code is
atomic within that context. If your context is a goroutine that doesn’t expose i to
other goroutines, then this code is atomic.

We can force atomicity by employing various techniques. The art then
becomes determining which areas of your code need to be atomic, and at what level
of granularity.

Memory access synchronization
----------------------------






Deadlocks, Livelocks, and Starvation
------------------------------------

A **deadlocked** program is one in which all concurrent processes are waiting on one
another. In this state, the program will never recover without outside intervention.

The Coffman Conditions for deadlocks to arise:

Mutual Exclusion
A concurrent process holds exclusive rights to a resource at any one time.
Wait For Condition
A concurrent process must simultaneously hold a resource and be waiting for an
additional resource.
No Preemption
A resource held by a concurrent process can only be released by that process, so
it fulfills this condition.
Circular Wait
A concurrent process (P1) must be waiting on a chain of other concurrent processes
(P2), which are in turn waiting on it (P1), so it fulfills this final condition
too.

If we ensure that at least one of these conditions is not true, we can prevent
deadlocks from occurring.

**Livelocks** are programs that are actively performing concurrent operations, but these
operations do nothing to move the state of the program forward.
Have you ever been in a hallway walking toward another person? She moves to one
side to let you pass, but you’ve just done the same. So you move to the other side, but
she’s also done the same. Imagine this going on forever, and you understand livelocks.

A very common reason livelocks are written: two or more concurrent processes attempting
to prevent a deadlock without coordination.

livelocks are more difficult to spot than deadlocks simply because it
can appear as if the program is doing work.

**Starvation**
Starvation is any situation where a concurrent process cannot get all the resources it
needs to perform work.


Determining Concurrency Safety
------------------------------

• Who is responsible for the concurrency?
• How is the problem space mapped onto concurrency primitives?
• Who is responsible for the synchronization?

For instance:

func CalculatePi(begin, end int63, pi (*)Pi)     
// CalculatePi calculates digits of Pi between the begin and end
// place.
//
// Internally, CalculatePi will create FLOOR((end-begin)/2) concurrent
// processes which recursively call CalculatePi. Synchronization of
// writes to pi are handled internally by the Pi struct.

We now understand that we can call the function plainly and not worry about concurrency
or synchronization.

we should instead take a functional approach and ensure our function
has no side effects:

func CalculatePi(begin, end int64) []uint

The signature of this function alone removes any questions of synchronization.

We can modify the signature again to throw out another signal as to what is happening:

func CalculatePi(begin, end int64) <-chan uint

This suggests that CalculatePi will at least have one goroutine and that we shouldn’t
bother with creating our own.

The way go encourages modeling your concurrent code encourages correctness, composability, 
and scalability.

The Difference Between Concurrency and Parallelism
--------------------------------------------------

Concurrency is a property of the code; parallelism is a property of the running
program.

The first is that we do not write parallel code, only concurrent code that we hope
will be run in parallel (porque igual solo hay un core disponible). Once again, 
parallelism is a property of the runtime of our program, not the code.

Now that many developers are working with distributed systems, it’s shifting back the
other way! We’re now beginning to think in terms of hypervisors, containers, and virtual
machines as our concurrent contexts.


the problem of modeling things concurrently is becoming both more difficult to reason about,
and more important.

the more difficult it is to get concurrency right, the
more important it is to have access to concurrency primitives that are easy to compose.
Unfortunately, most concurrent logic in our industry is written at one of the
highest levels of abstraction: OS threads.

we haven’t really added another layer of abstraction on top of OS threads,
we’ve supplanted them. we model things in goroutines and
channels, and occasionally shared memory.

How all this can help you.
-------------------------
As advancements in parallelism are made,
Go’s runtime will improve, as will the performance of your program—all for free.
Keep an eye on Go’s release notes and occasionally you’ll see things like:
In Go 1.5, the order in which goroutines are scheduled has been changed.
The Go authors are making improvements behind the scenes to make your program
faster.

Go’s Philosophy on Concurrency
------------------------------

Go routines
-----------

skiped for now...

The sync Package
----------------

- WaitGroup: 















