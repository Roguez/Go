En Ubuntu:

	1) Download from here the tar.gz
	2) Descompress using this:
	3) tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz
	
	donde debes cambiar VERSION, OS Y ARCH, POR EJEMPLO:
	
	go1.14.linux-amd64.tar.gz
	
   4)    Luego te vas a  $HOME/.profile y introduces esta linea:

          export PATH=$PATH:/usr/local/go/bin

 5) Haces un nuevo login para actualizar el cambio y listo.
	6) compruebas haciendo:
           go version
 7) Instalas tools con:
          sudo apt install golang-golang-x-tools

          Estas tools son necesarias para poder hacer por ejemplo:
          $godoc fmt Println 
          
          Que te muestra todas las funcionalidades de la built-in 
          function Println
